#Projet Mandelbrot
***
# Installation & Configuration

## QT

* Installer le sdk QT

```
        sudo apt-get install qt-sdk
```

* Télécharger l'archive sur le [site QT](https://www1.qt.io/download)

* Donner les droits d'accès a l'archive puis l'éxécuter

```
        chmod u+x "nom_de_l'archive.bin"
        ./"nom_de_l'archive.bin"
```

* Pendant l'installation ne pas oublier de cocher la bonne version (5.7) ainsi que le framework


## OpenGL
OpenGL permet un rendu direct et instantané des formes
Normalement OpenGL est installé sur la machine 

####Mise en place avec QT :

Ajouter ces lignes dans le fichier .pro :

```
        QT+=opengl
        LIBS += -lglut -lGLU
```

#### Tutoriel OpenGL / Qt

Le projet se base sur [ce tutoriel](http://fhalgand.developpez.com/tutoriels/qt/integration-opengl/01-premiere-application/)



## Cairo

Cairo permet de faire des enregistrement vectoriels.

####Installation

* Installer la librairie cairo

```
        sudo apt-get install libcairo2-dev
```
* Pour tester du code cairo en langage c,  [c'est par ici](https://cairographics.org/tutorial/)
* On oublie pas le include : 
```
        #include <cairo/cairo.h>
```
* **Attention lors de la compilation !** cairo.h n'est pas localisé dans "/usr/local/include/" mais dans /usr/local/include/cairo/ .  De plus, il faut indiquer au linker de chercher la librairie cairo. Pour compiler un fichier test.c on tape :
```
        g++ -I/usr/local/include/cairo/ -o test test.c -lcairo
```
```
        ./test
```
####Mise en place avec QT :
* Pour compiler du code cairo dans QT on ajoute les arguments dans le fichier .pro du projet. On doit au final obtenir un truc du genre:
```
    LIBS += -lglut -lGLU -I/usr/local/include/cairo/ -lcairo
```
* **Ne pas oublier les includes dans les fichiers sources !**



