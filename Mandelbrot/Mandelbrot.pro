SOURCES += \
    main.cpp \
    myglwidget.cpp \
    mainwindow.cpp \
    mandelbrot_window.cpp \
    julia_window_1.cpp \
    julia_window_2.cpp

QT += opengl
QT+=widgets

LIBS +=  -LGL -Lfreeglut -I/usr/local/include/cairo/ -lcairo

HEADERS += \
    myglwidget.h \
    mainwindow.h \
    mandelbrot_window.h \
    julia_window_1.h \
    julia_window_2.h
