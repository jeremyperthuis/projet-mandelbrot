#ifndef MENDELBROT_WINDOW_H
#define MENDELBROT_WINDOW_H
#include <QDebug>
#include "myglwidget.h"

class Mandelbrot_Window : public myGLWidget
{
    Q_OBJECT
public:
    explicit Mandelbrot_Window(QWidget *parent = 0);
    void initializeGL();
    void resizeGL(int width, int height);
    void paintGL();
    void perspective(float fovy, float aspect, float zNear, float zFar);
    void dessinerRepere(unsigned int echelle = 2);


public slots:
    void my_show();
};

#endif // MENDELBROT_WINDOW_H
