#ifndef JULIA_WINDOW_2_H
#define JULIA_WINDOW_2_H
#include "myglwidget.h"
#include <complex>

class Julia_Window_2 : public myGLWidget
{
    Q_OBJECT
public:
    explicit Julia_Window_2(QWidget *parent = 0);
    void initializeGL();
    void resizeGL(int width, int height);
    void paintGL();
    void perspective(float fovy, float aspect, float zNear, float zFar);
    void dessinerRepere(unsigned int echelle = 2);
    const std::complex<double>c = {(double)-0.577,(double)0.478};


};

#endif //
