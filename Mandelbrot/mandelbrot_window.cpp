﻿//////////////git+qt+zoom////////////
#include "mainwindow.h"
#include "mandelbrot_window.h"
#include<unistd.h>
#include <complex>
#include <iostream>


Mandelbrot_Window::Mandelbrot_Window(QWidget *parent)
    : myGLWidget(parent, (char*)"Ensemble de mandelbrot")
{
}

void Mandelbrot_Window::initializeGL()
{

    
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);        //initialisation de la profondeur
    glEnable(GL_POINT_SMOOTH);      //permet un lissage des points
    glEnable(GL_POINT_SIZE);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void Mandelbrot_Window::resizeGL(int width, int height)
{
    if(height == 0)
        height = 1;

    glViewport(0, 0, width, height);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
              /******* gluPerspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f); remplacer par perspective car inconnue pour qt5*/
    perspective(15.00f, (GLfloat)width/(GLfloat)height,0.1f, 100.0f);//arg1 zoom,arg2 largeur dessin,arg3 near distance oeil ecran,far distance oeil fond de l'image
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   glFlush();

}

void Mandelbrot_Window::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //on efface gl_depth_buffer_bit a chaque fois que la scene est dessiner
    glLoadIdentity();
    glPushMatrix();
   glTranslatef(0.4f, 0.0f, -7.00f);

   /*permet d'afficher un repere pour que l'utilisateur puisse zoomer ou bouger l'image*/

   dessinerRepere();

   /*******************Ensemble de Mandelbrot**********************/

   glBegin(GL_POINTS);

   for(double y=ymin; y<=ymax; y+=pas){

        for(double x=xmin; x<=xmax; x+=pas){

            //qDebug("x=%f y=%f",x,y);
            std::complex<double> z, c = {(double)(x/zoom_tmp)+direction_x,(double)(y/zoom_tmp)+direction_y};
            int iteration = 0;

            while(z.real()*z.real()+z.imag()*z.imag() < 4 && ++iteration < max_iteration)
                z = pow(z, 2) + c;

            if(iteration==max_iteration){
                glColor3ub(0,0,0);
                glVertex2d(x,y);
            }
            else{
                int b=(iteration*255)/(max_iteration/5);
                 glColor3ub(b,b,b);
                 glVertex2d(x,y);
            }
        }
    }
    glEnd();

    glPopMatrix();
    glFlush();

}
/****equivalent a gluPerspective()*****/
void Mandelbrot_Window::perspective(float fovy, float aspect, float zNear, float zFar)
{
    GLdouble xmin, xmax, ymin, ymax;

    ymax = zNear * tan( fovy * M_PI / 360.0 );
    ymin = -ymax;
    xmin = ymin * aspect;
    xmax = ymax * aspect;

    glFrustum( xmin, xmax, ymin, ymax, zNear, zFar );
}


void Mandelbrot_Window::dessinerRepere(unsigned int echelle)
{
    glPushMatrix();
    glScalef(echelle,echelle,echelle);
    glBegin(GL_LINES);
    glColor3ub(0,0,255);
    glVertex2d(0,0);
    glVertex2d(0.05,0);
    glColor3ub(0,255,0);
    glVertex2d(0,0);
    glVertex2d(0,0.05);
    glColor3ub(255,0,0);
    glVertex2d(0,0);
    glVertex2d(-0.05,0);
    glColor3ub(255,255,255);
    glVertex2d(0,0);
    glVertex2d(0,-0.05);

    glEnd();
    glPopMatrix();

}

/***********test signal show***********************/
void Mandelbrot_Window::my_show(){

    this->show();
}
