///// Ameliorer le style et fenetre pricipale///////

#include <QApplication>
#include "mandelbrot_window.h"
#include "mainwindow.h"
#include <cairo/cairo.h>

int main(int argc, char *argv[])
    {
        QApplication app(argc, argv);
        MainWindow w;
        QDesktopWidget *desktop = QApplication::desktop();

        /***********positionner la fenettre principale au centre de l'ecran******************************/

        int screenWidth = desktop->width();
        int screenHeight = desktop->height();

        int x = (screenWidth - w.width()) / 2;
        int y = (screenHeight - w.height()) / 2;

        w.move(x, y);
        w.show();

        return app.exec();
    }
