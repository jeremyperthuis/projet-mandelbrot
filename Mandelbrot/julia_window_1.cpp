#include "julia_window_1.h"
#include <unistd.h>


Julia_Window_1::Julia_Window_1(QWidget *parent)
    : myGLWidget(parent, (char*)"Ensemble de julia")
{
}

void Julia_Window_1::initializeGL()
{

    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);        //initialisation de la profondeur
    glEnable(GL_POINT_SMOOTH);      //permet un lissage des points
    glEnable(GL_POINT_SIZE);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void Julia_Window_1::resizeGL(int width, int height)
{
    if(height == 0)
        height = 1;

    glViewport(0, 0, width, height);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
              /******* gluPerspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f); remplacer par perspective car inconnue pour qt5*/
    perspective(20.00f, (GLfloat)width/(GLfloat)height,0.1f, 100.0f);//arg1 zoom,arg2 largeur dessin,arg3 near distance oeil ecran,far distance oeil fond de l'image
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   glFlush();
}

void Julia_Window_1::paintGL()
{

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //on efface gl_depth_buffer_bit a chaque fois que la scene est dessiner
    glLoadIdentity();
    glPushMatrix();
   glTranslatef(0.2f, 0.0f, -7.00f);

    //dessinerRepere();

    /******************************************julia***************************************/


    glBegin(GL_POINTS);



             for(double y=ymin; y<=ymax; y+=pas){

                for(double x=xmin; x<=xmax; x+=pas){
                    //qDebug("x=%f y=%f",x,y);
                    std::complex<double> z = {(double)(x/zoom_tmp)+direction_x,(double)(y/zoom_tmp)+direction_y};
                    int iteration = 0;
                    while(abs(z) < 2 && ++iteration < max_iteration)
                        z = pow(z, 2) + c;
                    if(iteration==max_iteration){

                        glVertex2d(x,y);

                        glColor3ub(0,0,0);

                    }
                    else{
                         glVertex2d(x,y);
                         glColor3ub(((iteration)*255)/(max_iteration/5),((iteration)*255)/(max_iteration/5),(iteration*255)/(max_iteration/5));
                    }
                }
            }
  glEnd();
  glPopMatrix();
  glFlush();

}

/**************************Fonction pour remplacer gluperspective****************************/
void Julia_Window_1::perspective(float fovy, float aspect, float zNear, float zFar)
{
    GLdouble xmin, xmax, ymin, ymax;

    ymax = zNear * tan( fovy * M_PI / 360.0 );
    ymin = -ymax;
    xmin = ymin * aspect;
    xmax = ymax * aspect;

    glFrustum( xmin, xmax, ymin, ymax, zNear, zFar );
}


void Julia_Window_1::dessinerRepere(unsigned int echelle)
{
    glPushMatrix();
    glScalef(echelle,echelle,echelle);
    glBegin(GL_LINES);
    glColor3ub(0,0,255);
    glVertex2d(0,0);
    glVertex2d(1,0);
    glColor3ub(0,255,0);
    glVertex2d(0,0);
    glVertex2d(0,1);
    glColor3ub(255,0,0);
    glVertex2d(0,0);
    glVertex2d(-1,0);
    glColor3ub(255,255,255);
    glVertex2d(0,0);
    glVertex2d(0,-1);

    glEnd();
    glPopMatrix();
}


