#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H

#include <QtOpenGL>
#include <QGLWidget>
#include <GL/glu.h>
#include <iostream>
#include <QMouseEvent>

class myGLWidget : public QGLWidget
{
    Q_OBJECT
public:
    explicit myGLWidget(QWidget *parent = 0, char *name = 0);
    virtual void initializeGL() = 0;
    virtual void resizeGL(int width, int height) = 0;
    virtual void paintGL() = 0;
    virtual void keyPressEvent( QKeyEvent *keyEvent );

    void drawInstructions(QPainter *painter);

    /*************cadre****************/

    const double xmin = -2.00f;
    const  double xmax = 1.00f;
    const double ymin = -1.00f;
    const double ymax = 1.00f;


    const int max_iteration =80;
    double direction_x;
    double direction_y;
    double zoom_tmp;  //
    const double zoom = 1.5;   //puissance du zoom
    const double pas= 0.005;
    bool b_Fullscreen;
    void toggleFullWindow();
public slots:
    virtual void timeOutSlot();

private:
    QTimer *t_Timer;



};


#endif // MYGLWIDGET_H
