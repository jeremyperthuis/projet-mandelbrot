#include "myglwidget.h"
#include <string>

myGLWidget::myGLWidget(QWidget *parent, char *name)
    : QGLWidget(parent)
{
    direction_x=0;
    direction_y=0;
    zoom_tmp =1;
    b_Fullscreen=false;

    setWindowTitle(QString::fromUtf8(name));

}

void myGLWidget::keyPressEvent(QKeyEvent *keyEvent)
{
    std::cout<<"zoom=x"<<zoom_tmp<<std::endl;
    switch(keyEvent->key())
    {
        case Qt::Key_Escape:
            close();
            break;

        case Qt::Key_Left:
          direction_x=direction_x-(0.2/zoom_tmp);
          updateGL();
          break;

       case Qt::Key_Right:
          direction_x=direction_x+(0.2/zoom_tmp);
          updateGL();
          break;

       case Qt::Key_Up:
          direction_y=direction_y+(0.2/zoom_tmp);
          updateGL();
          break;

       case Qt::Key_Down:
          direction_y=direction_y-(0.2/zoom_tmp);
          updateGL();
          break;

       case Qt::Key_A:
          zoom_tmp=zoom_tmp*zoom;
          updateGL();
          break;

    case Qt::Key_Q:
       zoom_tmp=zoom_tmp/zoom;
       if (zoom_tmp<1){
           zoom_tmp=1;
       }
       updateGL();
       break;

    case Qt::Key_Z:
        zoom_tmp=1;
        updateGL();
        break;


    case Qt::Key_F1:
                toggleFullWindow();
                break;

    }
}

void myGLWidget::timeOutSlot()
{
updateGL();
}

void myGLWidget::toggleFullWindow()
{
    if(b_Fullscreen)
    {
        showNormal();
        b_Fullscreen = false;
    }
    else
    {
        showFullScreen();
        b_Fullscreen = true;
    }
}


