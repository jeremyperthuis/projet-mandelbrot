#include "mainwindow.h"
#include "mandelbrot_window.h"
#include "julia_window_1.h"
#include "julia_window_2.h"
#include <iostream>
#include <string>

MainWindow::MainWindow()   //Probleme bordure
{

        // Creation Widget principal
        QWidget *zoneCentrale = new QWidget;

        // Creation boutons choix
        QPushButton *BoutonMandelbrot = new QPushButton("Ensemble de Mandelbrot", zoneCentrale);
        QPushButton *BoutonJulia = new QPushButton("Ensemble de Julia->(c = −0.0519 + i0.688)", zoneCentrale);
        QPushButton *BoutonJulia2 = new QPushButton("Ensemble de Julia->(c = −0.577 + i0.478)", zoneCentrale);


        // Enleve les bordures des boutons
        BoutonMandelbrot->setFlat(false);
        BoutonJulia->setFlat(false);
        BoutonJulia2->setFlat(false);

        // Taille fenetres boutons
        //BoutonMandelbrot->setFixedSize(500,500);
        //BoutonJulia->setFixedSize(500,500);
        //BoutonJulia2->setFixedSize(width()/2,500);

        // Police Texte Bouton
        QFont f( "Arial",30, QFont::Bold);
        BoutonMandelbrot->setFont( f);
        BoutonJulia->setFont( f);
        BoutonJulia2->setFont( f);

        // Creation layout & alignement
        QVBoxLayout *layout = new QVBoxLayout;
        layout->addWidget(BoutonMandelbrot);
        layout->addWidget(BoutonJulia);
        layout->addWidget(BoutonJulia2);

        zoneCentrale->setLayout(layout);
        setCentralWidget(zoneCentrale);


        Mandelbrot_Window *Mw = new Mandelbrot_Window();
        Julia_Window_1 *Jw = new Julia_Window_1();
        Julia_Window_2 *Jw2 = new Julia_Window_2();


        // On ferme la fenetre de choix puis on ouvre l'ensemble
        QObject::connect(BoutonMandelbrot, SIGNAL(clicked()), this, SLOT(close()));
        QObject::connect(BoutonMandelbrot, SIGNAL(clicked()), Mw, SLOT(my_show()));

        QObject::connect(BoutonJulia, SIGNAL(clicked()), this, SLOT(close()));
        QObject::connect(BoutonJulia, SIGNAL(clicked()), Jw, SLOT(show()));

        QObject::connect(BoutonJulia2, SIGNAL(clicked()), this, SLOT(close()));
        QObject::connect(BoutonJulia2, SIGNAL(clicked()), Jw2, SLOT(show()));


}



